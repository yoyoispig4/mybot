### Run Server

- go run ./cmd/service/main.go

### Run Ngrok

- ngrok http 3000

### Config Setting

- refer to env_example

## Run Mongo(docker-compose)

- ./script/start_mongo.sh

### TODO

- [ ] unit test
- [ ] cobra
