package user

import "time"

type UserModel struct {
	Id      string    `json:"_id"`
	Name    string    `json:"name"`
	UserID  string    `json:"UserID"`
	Message string    `json:"message"`
	Time    time.Time `json:"time"`
}
