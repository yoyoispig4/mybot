package mongodb

import (
	"context"
	"fmt"
	"log"
	"main/cmd/config"
	"main/cmd/model/user"

	"time"

	"github.com/michael-wang/envar"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MyMongo struct {
	client *mongo.Client
}

type MongoInterFace interface {
	InsertData(ctx context.Context, db string, table string, name string, id string, message string) (string, error)
	FindDataByUserName(ctx context.Context, db string, table string, name string) ([]user.UserModel, error)
}

var MongoClient *MyMongo

func GetClient() MongoInterFace {
	if MongoClient == nil {

		MongoClient = &MyMongo{}
		ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		client, err := mongo.Connect(ctx, options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s@%s:%s/?authSource=admin", envar.String(config.MongoAccount), envar.String(config.MongoPassword), envar.String(config.MongoHost), envar.String(config.MongoPort))))
		if err != nil {
			panic(err)
		}
		MongoClient.client = client
	}
	return MongoClient
}

func (r *MyMongo) InsertData(ctx context.Context, db string, table string, name string, id string, message string) (string, error) {
	collection := r.client.Database(db).Collection(table)

	res, err := collection.InsertOne(ctx, bson.M{
		"name":    name,
		"UserId":  id,
		"message": message,
		"time":    time.Now(),
	})

	if err != nil {
		return "", err
	}
	oid, ok := res.InsertedID.(primitive.ObjectID)
	if !ok {
		return "", err
	}

	return oid.Hex(), nil

}

func (r *MyMongo) FindDataByUserName(ctx context.Context, db string, table string, name string) ([]user.UserModel, error) {
	collection := r.client.Database(db).Collection(table)

	// var UsersModel []user.UserModel

	cur, err := collection.Find(ctx, bson.M{"name": name})
	if err != nil {
		log.Fatal(err)
	}
	defer cur.Close(ctx)

	var users []user.UserModel
	for cur.Next(ctx) {
		var user user.UserModel
		err := cur.Decode(&user)
		if err != nil {
			return nil, err
		}
		users = append(users, user)

	}
	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	return users, nil

}
