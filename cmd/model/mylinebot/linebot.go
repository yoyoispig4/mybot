package mylinebot

import (
	"fmt"
	"log"
	"main/cmd/config"

	"github.com/line/line-bot-sdk-go/linebot"
	"github.com/michael-wang/envar"
)

var MyBot *linebot.Client

func GetClient() *linebot.Client {
	fmt.Println(envar.String(config.LineSecret))
	fmt.Println(envar.String(config.LineToken))
	MyBot, err := linebot.New(
		envar.String(config.LineSecret),
		envar.String(config.LineToken),
	)
	if err != nil {
		log.Fatal(err)
	}
	return MyBot
}
