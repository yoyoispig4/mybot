package main

import (
	// "fmt"
	"fmt"
	"log"
	"net/http"

	"main/cmd/model/mongodb"
	"main/cmd/model/mylinebot"

	"github.com/gin-gonic/gin"
	"github.com/line/line-bot-sdk-go/linebot"
)

var myBot = mylinebot.GetClient()
var myMongo = mongodb.GetClient()

func main() {

	r := gin.Default()
	r.POST("/callback", CallbackHandler())
	r.GET("/user/:name", GetUserMessage())

	r.Run(":3000")

}

func GetUserMessage() gin.HandlerFunc {
	return func(context *gin.Context) {
		name := context.Param("name")
		fmt.Println(name)
		data, err := myMongo.FindDataByUserName(context, "testing", "user", name)
		if err != nil {
			context.String(http.StatusInternalServerError, err.Error())
		} else {
			context.String(http.StatusOK, fmt.Sprintln(data))
		}

	}
}

func CallbackHandler() gin.HandlerFunc {
	return func(context *gin.Context) {
		events, err := myBot.ParseRequest(context.Request)
		if err != nil {
			if err == linebot.ErrInvalidSignature {
				context.JSON(http.StatusBadRequest, nil)
			} else {
				context.JSON(http.StatusInternalServerError, nil)
			}
			return
		}

		for _, event := range events {
			switch event.Type {
			case linebot.EventTypeMessage:
				// fmt.Println(event.)

				profile, err := myBot.GetProfile(event.Source.UserID).Do()
				if err != nil {
					log.Print(err)
				}

				switch message := event.Message.(type) {
				case *linebot.TextMessage:
					matchText := message.Text
					myMongo.InsertData(context, "testing", "user", profile.DisplayName, profile.UserID, matchText)
					if _, err = myBot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage(message.Text)).Do(); err != nil {
						log.Println(err.Error())
					}

				case *linebot.ImageMessage:
					log.Print(message)
				case *linebot.VideoMessage:
					log.Print(message)
				case *linebot.AudioMessage:
					log.Print(message)
				case *linebot.FileMessage:
					log.Print(message)
				case *linebot.LocationMessage:
					log.Print(message)
				case *linebot.StickerMessage:
					log.Print(message)
				default:
					log.Printf("Unknown message: %v", message)
				}
			default:
				log.Printf("Unknown event: %v", event)
			}
		}
		context.JSON(http.StatusOK, gin.H{
			"success": events,
		})
	}
}
