package config

import (
	"io/ioutil"
	"log"
	"os"

	"github.com/michael-wang/envar"
)

const (
	DbHost = "DB_HOST"
	DbUser = "DB_USER"

	LineSecret = "Line_Secret"
	LineToken  = "Line_Token"

	EnvLogInfo = "LOG_INFO"

	MongoHost     = "MONGO_HOST"
	MongoPort     = "MONGO_PORT"
	MongoAccount  = "MONGO_ACCOUNT"
	MongoPassword = "MONGO_PASSWORD"
)

var InfoLog *log.Logger
var ErrLog *log.Logger

func init() {
	// setup env var (環境變數，用來控制 server 行為)

	envar.Load()

	envar.SetDef(DbHost, "")
	envar.SetDef(DbUser, "")
	envar.SetDef(LineSecret, "")
	envar.SetDef(LineToken, "")

	envar.SetDef(MongoHost, "")
	envar.SetDef(MongoPort, "")

	envar.SetDef(MongoAccount, "")
	envar.SetDef(MongoPassword, "")

	envar.SetDef(EnvLogInfo, false)

	InfoLog = log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	ErrLog = log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)
	if !envar.Bool(EnvLogInfo) {
		InfoLog.SetFlags(0)
		InfoLog.SetOutput(ioutil.Discard)
	}

}
